package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/16:26:08
 */
public interface CustomerService {

    Map<String, Object> list(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Integer customerId, Customer customer);

    void delete(String ids);
}
