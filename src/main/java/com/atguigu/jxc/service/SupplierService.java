package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/13:31:19
 */

public interface SupplierService {

    Map<String, Object> list(Integer page, Integer rows, String supplierName);

     void saveOrUpdate(Integer supplierId, Supplier supplier);

    void delete(String ids);
}
