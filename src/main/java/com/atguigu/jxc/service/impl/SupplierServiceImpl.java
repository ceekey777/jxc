package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/13:31:32
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        PageHelper.startPage(page, rows);
        Map<String, Object> result = new HashMap<>();

        List<Supplier> supplierList = supplierDao.list(page,rows,supplierName);
        PageInfo<Supplier> pageInfo = PageInfo.of(supplierList);

        result.put("total", pageInfo.getTotal());
        result.put("rows", pageInfo.getList());

        return result;
    }

    @Override
    public void saveOrUpdate(Integer supplierId, Supplier supplier) {
        if (supplierId == null){
            supplierDao.save(supplier);
        }else {
            supplierDao.update(supplierId,supplier);
        }

    }

    @Override
    public void delete(String ids) {
        // String[] idArr = ids.split(",");
        // List<Integer> list = new ArrayList<>();
        // for (String id : idArr) {
        //     int idList = Integer.parseInt(id);
        //     list.add(idList);
        // }
        // supplierDao.delete(list);

        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        List<Integer> list = idList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());
        supplierDao.delete(list);
    }
}
