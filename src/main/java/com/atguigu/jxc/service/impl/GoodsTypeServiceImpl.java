package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));
        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    @Override
    public void saveOrUpdate(String goodsTypeName, Integer pId) {
        GoodsType goodsType = new GoodsType(goodsTypeName, 0, pId);
        goodsTypeDao.save(goodsType);
        GoodsType parentGoodsType = goodsTypeDao.getGoodsTypeById(pId);

        if (parentGoodsType.getGoodsTypeState() == 0) {
            parentGoodsType.setGoodsTypeState(1);
            goodsTypeDao.updateGoodsTypeState(parentGoodsType);
        }
    }

    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        // 根据商品类别ID来查询商品信息，如果该类别下有商品信息，则不给予删除
        List<GoodsType> goodsList = goodsTypeDao.getGoodsByTypeId(goodsTypeId);
        if (goodsList.size() != 0) {
            return new ServiceVO<>(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        // 这里的逻辑是先根据商品类别ID查询出商品类别的信息，找到商品类别的父级商品类别
        // 如果父商品类别的子商品类别信息等于1，那么再删除商品信息的时候，父级商品类别的状态也应该从根节点改为叶子节点
        GoodsType goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId);
        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getPId());
        if (goodsTypeList.size() == 1){
            GoodsType parentGoodsType = goodsTypeDao.getGoodsTypeById(goodsType.getPId());
            parentGoodsType.setGoodsTypeState(0);
            goodsTypeDao.updateGoodsTypeState(parentGoodsType);
        }

        goodsTypeDao.delete(goodsTypeId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId) {

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for (int i = 0; i < array.size(); i++) {

            HashMap obj = (HashMap) array.get(i);

            if (obj.get("state").equals("open")) {// 如果是叶子节点，不再递归

            } else {// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     *
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId) {

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        // 遍历商品类别
        for (GoodsType goodsType : goodsTypeList) {

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if (goodsType.getGoodsTypeState() == 1) {
                obj.put("state", "closed");

            } else {
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}
