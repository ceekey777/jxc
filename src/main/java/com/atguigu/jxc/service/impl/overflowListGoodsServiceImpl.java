package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.bytebuddy.description.method.MethodDescription;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/08/8:32:33
 */
@Service
public class overflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    GoodsDao goodsDao;
    @Autowired
    UserDao userDao;

    @Override
    public void save(OverflowList overflowList, String overflowListGoodsStr) {

        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());

        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        overflowList.setUserId(currentUser.getUserId());

        overflowListGoodsDao.saveOverflowList(overflowList);

// 保存商品报溢单商品信息
        for(OverflowListGoods overflowListGoods : overflowListGoodsList) {

            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());

            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.getGoodsById(overflowListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity() + overflowListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.update(goods.getGoodsId(), goods);
        }

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowList = overflowListGoodsDao.list(sTime,eTime);

        map.put("rows",overflowList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowList = overflowListGoodsDao.goodsList(overflowListId);
        map.put("rows",overflowList);
        return map;
    }
}
