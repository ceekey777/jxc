package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/16:26:22
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        PageHelper.startPage(page,rows);
        Map<String,Object> result = new HashMap<>();

        List<Customer> customerList = customerDao.list(page,rows,customerName);
        PageInfo<Customer> pageInfo = PageInfo.of(customerList);

        result.put("rows",pageInfo.getList());
        result.put("total",pageInfo.getTotal());
        return  result;
    }

    @Override
    public void saveOrUpdate(Integer customerId, Customer customer) {
        if (customerId == null){
            customerDao.save(customer);
        }else{
            customerDao.update(customerId,customer);
        }
    }

    @Override
    public void delete(String ids) {
        List<String> idList = Arrays.stream(ids.split(",")).collect(Collectors.toList());
        List<Integer> list = idList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());
        customerDao.delete(list);
    }
}
