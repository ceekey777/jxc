package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/20:25:34
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    DamageListGoodsDao damageListGoodsDao;

    @Autowired
    GoodsDao goodsDao;
    @Autowired
    UserDao userDao;

    @Override
    public void save(DamageList damageList, String damageListGoodsStr) {
        Gson gson = new Gson();

        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr,new TypeToken<List<DamageListGoods>>(){}.getType());

        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        damageList.setUserId(currentUser.getUserId());

        // 保存商品报损单信息
        damageListGoodsDao.saveDamageList(damageList);

        // 保存商品报损单商品信息
        for(DamageListGoods damageListGoods : damageListGoodsList){

            damageListGoods.setDamageListId(damageList.getDamageListId());

            damageListGoodsDao.saveDamageListGoods(damageListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.getGoodsById(damageListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity()-damageListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.update(goods.getGoodsId(), goods);

        }
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageList = damageListGoodsDao.list(sTime,eTime);

        map.put("rows",damageList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageGoodsList = damageListGoodsDao.goodsList(damageListId);
        map.put("rows",damageGoodsList);
        return map;
    }
}
