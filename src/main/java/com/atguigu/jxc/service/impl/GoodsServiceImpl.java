package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.ErrorMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    SaleListGoodsService saleListGoodsService;
    @Autowired
    CustomerReturnListGoodsService customerReturnListGoodsService;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        PageHelper.startPage(page,rows);
        Map<String, Object> result = new HashMap<>();

        List<Goods> goodsList = goodsDao.listInventory(page,rows,codeOrName, goodsTypeId);
        PageInfo<Goods> pageInfo = PageInfo.of(goodsList);
        result.put("total", pageInfo.getTotal());
        result.put("rows", pageInfo.getList());
        return result;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        PageHelper.startPage(page,rows);
        HashMap<String, Object> result = new HashMap<>();
        List<Goods> goodsList = goodsDao.list(page,rows,goodsName, goodsTypeId);
        PageInfo<Goods> pageInfo = PageInfo.of(goodsList);
        result.put("total", pageInfo.getTotal());
        result.put("rows", pageInfo.getList());
        return result;
    }

    @Override
    public void saveOrUpdate(Integer goodsId, Goods goods) {
        if (goodsId == null){
            //新增
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goods.setLastPurchasingPrice(0);
            goodsDao.save(goods);
        }else {
            //修改
            goodsDao.update(goodsId,goods);
        }
    }

    @Override
    public ServiceVO delete(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);

        if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }else if(goods.getState() == 1){
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }else{
            goodsDao.delete(goodsId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        PageHelper.startPage(page,rows);
        Map<String, Object> result = new HashMap<>();
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(page,rows,nameOrCode);
        PageInfo<Goods> pageInfo = PageInfo.of(goodsList);
        result.put("total",pageInfo.getTotal());
        result.put("rows",pageInfo.getList());
        return result;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        PageHelper.startPage(page,rows);
        Map<String, Object> result = new HashMap<>();
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(page,rows,nameOrCode);
        PageInfo<Goods> pageInfo = PageInfo.of(goodsList);
        result.put("total",pageInfo.getTotal());
        result.put("rows",pageInfo.getList());
        return result;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        goods.setPurchasingPrice(purchasingPrice);
        goods.setInventoryQuantity(inventoryQuantity);
        goodsDao.saveStock(goodsId,goods);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);

        if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }else {
            goods.setInventoryQuantity(0);
            goodsDao.deleteStock(goodsId,goods);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> result = new HashMap<>();
        List<Goods> list = goodsDao.listAlarm();
        result.put("rows",list);
        return result;
    }

}
