package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/8:43:22
 */
public interface UnitService {
    Map<String, Object> list();
}
