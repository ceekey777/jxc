package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/20:25:19
 */
public interface DamageListGoodsService {
    void save(DamageList damageList, String damageListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
