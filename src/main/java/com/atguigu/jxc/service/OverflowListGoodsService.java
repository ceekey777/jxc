package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/08/8:32:21
 */
public interface OverflowListGoodsService {
    void save(OverflowList overflowList, String overflowListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
