package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/8:38:03
 */
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    UnitService unitService;

    /**
     * 请求URL：http://localhost:8080/unit/list
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String,Object> list(){
        Map<String,Object> list = unitService.list();
        return list;
    }
}
