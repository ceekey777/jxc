package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/16:23:20
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    /**
     * 请求URL：http://localhost:8080 /customer/list
     * 请求参数：Integer page, Integer rows, String  customerName
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page") Integer page,
                                   @RequestParam("rows")Integer rows,
                                   @RequestParam(value = "customerName",required = false)String customerName){

            Map<String,Object> result = customerService.list(page,rows,customerName);
            return result;
    }

    /**
     * 请求URL：http://localhost:8080/ customer/save?customerId=1
     * 请求参数：Customer customer
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "customerId",required = false)Integer customerId,
                                  Customer customer){

        customerService.saveOrUpdate(customerId,customer);

        return new ServiceVO(100,"操作成功");
    }

    /**
     * 请求URL：http://localhost:8080/customer/delete
     * 请求参数：String  ids
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("delete")
    public ServiceVO delete(String  ids){
            customerService.delete(ids);
        return new ServiceVO<>(100,"删除成功!");
    }
}
