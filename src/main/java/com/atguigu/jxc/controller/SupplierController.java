package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/13:28:49
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam(value = "supplierName",required = false) String supplierName){
        Map<String,Object> result = supplierService.list(page,rows,supplierName);
        return result;
    }

    //http://localhost:8080/supplier/save?supplierId=1
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "supplierId",required = false)Integer supplierId,
                                  Supplier supplier){
         supplierService.saveOrUpdate(supplierId,supplier);
         return new ServiceVO(200,"操作成功");
    }

    //http://localhost:8080/supplier/delete
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
        supplierService.delete(ids);
        return  new ServiceVO(100,"删除成功");
    }
}


