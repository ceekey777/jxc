package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息 http://localhost:8080/goods/listInventory
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> listInventory(@RequestParam("page") Integer page,
                                             @RequestParam("rows") Integer rows,
                                             @RequestParam(value = "codeOrName", required = false) String codeOrName,
                                             @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {

        Map<String, Object> result = goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
        return result;
    }

    /**
     * 请求URL：http://localhost:8080/goods/list
     * 请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam("page") Integer page,
                                    @RequestParam("rows") Integer rows,
                                    @RequestParam(value = "goodsName", required = false) String goodsName,
                                    @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {

        Map<String, Object> result = goodsService.list(page, rows, goodsName, goodsTypeId);
        return result;

    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "goodsId", required = false) Integer goodsId,
                                  Goods goods) {
        goodsService.saveOrUpdate(goodsId,goods);
        return new ServiceVO<>(100, "操作成功");
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(Integer goodsId){
        ServiceVO result = goodsService.delete(goodsId);
        if (result.getCode() == SuccessCode.SUCCESS_CODE) {
            return new ServiceVO<>(100, "删除成功!");
        } else {
            return new ServiceVO<>(result.getCode(), result.getMsg());
        }
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam("page") Integer page,
                                                     @RequestParam("rows") Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);

    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(@RequestParam("page") Integer page,
                                                     @RequestParam("rows") Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);

    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        ServiceVO result = goodsService.deleteStock(goodsId);
        if (result.getCode() == SuccessCode.SUCCESS_CODE) {
            return new ServiceVO<>(100, "删除成功!");
        } else {
            return new ServiceVO<>(result.getCode(), result.getMsg());
        }
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm(){
        Map<String,Object> result = goodsService.listAlarm();
        return result;
    }
}
