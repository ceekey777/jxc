package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/20:23:28
 */
@RestController
@RequestMapping("/damageListGoods")
public class damageListGoodsController {

    @Autowired
    DamageListGoodsService damageListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr){
        damageListGoodsService.save(damageList,damageListGoodsStr);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


    @PostMapping("/list")
    public Map<String,Object> list(String sTime, String eTime){
        Map<String,Object> result = damageListGoodsService.list(sTime,eTime);
        return result;
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        Map<String,Object> result = damageListGoodsService.goodsList(damageListId);
        return result;
    }
}
