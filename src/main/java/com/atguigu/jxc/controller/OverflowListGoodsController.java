package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/08/8:27:19
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr){
        overflowListGoodsService.save(overflowList,overflowListGoodsStr);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/list")
    public Map<String,Object> list(String sTime, String eTime){
        Map<String,Object> result = overflowListGoodsService.list(sTime,eTime);
        return result;
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        Map<String,Object> result = overflowListGoodsService.goodsList(overflowListId);
        return result;
    }
}
