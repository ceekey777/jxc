package com.atguigu.jxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 客户实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

  private Integer customerId;
  private String customerName;
  private String contacts;
  private String phoneNumber;
  private String address;
  private String remarks;

}
