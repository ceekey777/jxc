package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/16:30:41
 */

public interface CustomerDao {

    List<Customer> list(Integer page, Integer rows, String customerName);

    void save(Customer customer);

    void update(@Param("customerId") Integer customerId, @Param("customer") Customer customer);

    void delete(List<Integer> list);
}
