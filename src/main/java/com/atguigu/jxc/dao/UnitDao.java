package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/8:44:11
 */
public interface UnitDao {
    List<Unit> list();
}
