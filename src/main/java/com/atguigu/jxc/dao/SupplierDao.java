package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/05/13:36:39
 */

public interface SupplierDao {

    List<Supplier> list(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier);

    void update(@Param("supplierId") Integer supplierId, @Param("supplier") Supplier supplier);

    void delete(List<Integer> ids);
}
