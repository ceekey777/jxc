package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description 商品信息
 */
@Component
public interface GoodsDao {


    String getMaxCode();

    List<Goods> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    List<Goods> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void save(Goods goods);

    void update(Integer goodsId, Goods goods);

    void delete(Integer goodsId);

    Goods getGoodsById(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Goods goods);

    void deleteStock(Integer goodsId, Goods goods);

    List<Goods> listAlarm();
}
