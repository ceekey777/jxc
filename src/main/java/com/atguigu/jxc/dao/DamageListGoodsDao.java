package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/07/20:44:55
 */
@Component
public interface DamageListGoodsDao {

    void saveDamageList(DamageList damageList);

    void saveDamageListGoods(DamageListGoods damageListGoods);

    List<DamageList> list(String sTime, String eTime);

    List<DamageList> goodsList(Integer damageListId);
}
