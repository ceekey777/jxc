package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @params:
 * @return:
 * @Author: Ceekey  Email:147464823@qq.com
 * @Date: 2023/08/08/8:33:24
 */
public interface OverflowListGoodsDao {
    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods overflowListGoods);

    List<OverflowList> list(String sTime, String eTime);

    List<OverflowList> goodsList(Integer overflowListId);
}
